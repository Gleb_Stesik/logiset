from django.shortcuts import render
from django.views import generic
from .models import GalleryImage
from django.shortcuts import render_to_response, get_object_or_404

# Create your views here.
class GalleryView(generic.ListView):
    template_name = 'gallery.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        gallery = GalleryImage.objects.all ()
        return gallery