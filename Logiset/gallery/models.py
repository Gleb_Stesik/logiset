from django.db import models

class GalleryImage(models.Model):
    headline_gallery = models.CharField(max_length=200, db_index=True, verbose_name="Название изображения")
    gallery_image = models.ImageField(upload_to='media/%Y/%m/%d/', blank=True, verbose_name="Изображение")
    gallery_description = models.TextField(blank=True, verbose_name="Описание")

    def __str__(self):
        return '{0.headline_gallery}'.format(self)