from django.db import models

class News(models.Model):
    headline_news = models.CharField(max_length=200, db_index=True, verbose_name="Заголовок новости")
    image = models.ImageField(upload_to='media/%Y/%m/%d/', blank=True, verbose_name="Изображение")
    description = models.TextField(blank=True, verbose_name="Описание")

    def __str__(self):
        return '{0.headline_news}'.format(self)