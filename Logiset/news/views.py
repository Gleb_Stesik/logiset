from django.shortcuts import render
from django.views import generic
from .models import News
from django.shortcuts import render_to_response, get_object_or_404

# Create your views here.
class AboutUsView(generic.ListView):
    template_name = 'about_us.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class UslugiView(generic.ListView):
    template_name = 'uslugi.html'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class ProduktyView(generic.ListView):
    template_name = 'produkty.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class RazmotkaView(generic.ListView):
    template_name = 'razmotka.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class PoperechnajaView(generic.ListView):
    template_name = 'poperechnaja.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class ProdolnajaView ( generic.ListView ):
    template_name = 'prodolnaja.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class PokritieView(generic.ListView):
    template_name = 'pokritie.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class NerzView(generic.ListView):
    template_name = 'nerz.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class LegirView(generic.ListView):
    template_name = 'legir.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news


class TitanView(generic.ListView):
    template_name = 'titan.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news

class FirstGrade(generic.ListView):
    template_name = 'second_grade.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news

class SecondGrade(generic.ListView):
    template_name = 'first_grade.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all ()
        return news

class NewsView(generic.ListView):
    template_name = 'news.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all()
        return news

class ContactView(generic.ListView):
    template_name = 'contact.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        news = News.objects.all()
        return news




