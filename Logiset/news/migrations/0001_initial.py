# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-03-13 18:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headline_news', models.CharField(db_index=True, max_length=200, verbose_name='Заголовок новости')),
                ('image', models.ImageField(blank=True, upload_to='media/%Y/%m/%d/', verbose_name='Изображение')),
                ('description', models.TextField(blank=True, verbose_name='Описание')),
            ],
        ),
    ]
