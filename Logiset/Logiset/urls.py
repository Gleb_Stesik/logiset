"""Logiset URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from news.views import *
from gallery.views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', AboutUsView.as_view(), name='home'),
    url(r'^uslugi/razmotka/$', RazmotkaView.as_view (), name='razmotka' ),
    url(r'^uslugi/poperechnaja/$', PoperechnajaView.as_view(), name='poperechnaja' ),
    url(r'^uslugi/prodolnaja/$', ProdolnajaView.as_view(), name='prodolnaja' ),
    url(r'^uslugi/pokritie/$', PokritieView.as_view(), name='pokritie' ),
    url(r'^produkty/produkty/$', ProduktyView.as_view(), name='produkty' ),
    url(r'^produkty/nerz/$', NerzView.as_view(), name='nerz' ),
    url(r'^produkty/legir/$', LegirView.as_view(), name='legir' ),
    url(r'^produkty/titan/$', TitanView.as_view(), name='titan' ),
    url(r'^uslugi/$', UslugiView.as_view(), name='uslugi' ),
    url(r'^produkty/$', ProduktyView.as_view(), name='produkty' ),
    url(r'^gallery/$', GalleryView.as_view(), name='gallery' ),
    url(r'^first_grade/$', FirstGrade.as_view(), name='gallery' ),
    url(r'^second_grade/$', SecondGrade.as_view(), name='gallery' ),
    url(r'^news/$',NewsView.as_view (), name='news' ),
    url(r'^contact/$', ContactView.as_view(), name='news' ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT )

urlpatterns += staticfiles_urlpatterns()